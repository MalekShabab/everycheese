FROM gitpod/workspace-postgres

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

ENV POSTGRES_PASSWORD="nuttertools"
ENV POSTGRES_USER="postgres"
ENV POSTGRES_DB="everycheese"
